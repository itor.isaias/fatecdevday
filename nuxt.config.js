export default {
  head: {
    title: 'Fatec Dev Day 2019',
    htmlAttrs: {
      lang: 'pt-br'
    },
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  meta: {
    lang: 'pt-br',
    name: 'Fatec Dev Day 2019',
    description: 'Fatec Dev Day: um dia dedicado a desenvolvimento Web, aplicativos e sistemas em geral'
  },
  loading: { color: '#ad0f0a' },
  generate: {
    fallback: true
  },
  ignore: [
    '**/.netlify/**'
  ],
  modules: [
    '@nuxtjs/pwa'
  ],
  manifest: {
    name: 'Fatec Dev Day 2019',
    short_name: 'Fatec Dev Day',
    lang: 'pt-br',
    start_url: '/',
    description: 'Fatec Dev Day: um dia dedicado a desenvolvimento Web, aplicativos e sistemas em geral'
  },
  build: {
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
