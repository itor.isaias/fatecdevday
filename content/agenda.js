import talks from './talks'

// export default [
//   {
//     id: 1,
//     icon: require('~/assets/icons/assignment.svg'),
//     time: '07:30 - 08:00',
//     duration: '30min',
//     title: 'Credenciamento',
//     description: 'O credenciamento no evento é obrigatório e não ocorre além do horário estipulado.'
//   },

//   {
//     id: 2,
//     icon: require('~/assets/icons/mic.svg'),
//     time: '08:00 - 08:10',
//     duration: '10min',
//     title: 'Abertura',
//     description: 'Boas vindas da organização e agradecimentos aos patrocinadores.'
//   },

//   {
//     id: 3,
//     time: '08:10 - 09:05',
//     duration: '55min',
//     ...talks.find(talk => talk.speaker.name === 'Sérgio Gama')
//   },

//   {
//     id: 4,
//     time: '09:10 - 10:00',
//     duration: '50min',
//     ...talks.find(talk => talk.speaker.name === 'Raphael Mantilha')
//   },

//   {
//     id: 5,
//     time: '10:05 - 10:55',
//     duration: '50min',
//     ...talks.find(talk => talk.speaker.name === 'Samuel Martins')
//   },

//   {
//     id: 6,
//     time: '11:00 - 12:00',
//     duration: '60min',
//     ...talks.find(talk => talk.speaker.name === 'Danilo Deus Dará')
//   },

//   {
//     id: 7,
//     icon: require('~/assets/icons/lunch.svg'),
//     time: '12:00 - 13:30',
//     duration: '1h30min',
//     title: 'Almoço',
//     description: 'O intervalo para almoço é livre, cada participante pode escolher onde fazer sua refeição.'
//   },

//   {
//     id: 8,
//     time: '13:30 - 14:30',
//     duration: '60min',
//     ...talks.find(talk => talk.speaker.name === 'Jota Júnior')
//   },

//   {
//     id: 9,
//     time: '14:35 - 15:30',
//     duration: '55min',
//     ...talks.find(talk => talk.speaker.name === 'Igor Halfeld')
//   },

//   {
//     id: 10,
//     icon: require('~/assets/icons/breakfast.svg'),
//     time: '15:30 - 16:00',
//     duration: '30min',
//     title: 'Coffee Break',
//     description: 'Breve intervalo para descanço dentro do próprio evento.'
//   },

//   {
//     id: 11,
//     time: '16:00 - 17:00',
//     duration: '60min',
//     ...talks.find(talk => talk.speaker.name === 'Vilibaldo Neto')
//   },

//   {
//     id: 12,
//     time: '17:05 - 18:00',
//     duration: '55min',
//     ...talks.find(talk => talk.speaker.name === 'Cleber Campomori')
//   },

//   {
//     id: 13,
//     icon: require('~/assets/icons/giftcard.svg'),
//     time: '18:00 - 18:10',
//     duration: '10min',
//     title: 'Sorteios',
//     description: `Distribuição de benefícios oferecidos por nossos parceiros apoiadores do evento.`
//   },

//   {
//     id: 14,
//     icon: require('~/assets/icons/leave.svg'),
//     time: '18:10',
//     title: 'Encerramento',
//     code: `function FatecDevDay () { return 'fim'; }`
//   }
// ]

export default [

]
